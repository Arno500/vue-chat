import Vue from 'vue'
import Vuex from 'vuex'
import socketio from 'socket.io-client'

import router from './router'

Vue.use(Vuex)
let sock = socketio('https://bddi-2019-chat.herokuapp.com/')

const ICONS = [
  '/avatars/clic.svg',
  '/avatars/yo.svg',
  '/avatars/fuck.svg',
  '/avatars/like.svg'
]
// let sock = socketio('http://localhost:3000/')

const store = new Vuex.Store({
  state: {
    users: [],
    messages: [],
    user: {
      username: sessionStorage.getItem('username'),
      avatar: sessionStorage.getItem('avatar'),
      isLogged: false
    },
    loading: false,
    poke: 0
  },
  getters: {
    isLogged (state) {
      return state.user.isLogged
    },
    loading (state) {
      return state.loading
    },
    users (state) {
      return state.users.filter(elm => elm.username !== state.user.username)
    },
    typers (state) {
      return state.users.filter(
        elm => elm.typing === true && elm.username !== state.user.username
      )
    }
  },
  mutations: {
    loginUser (state, user) {
      state.user = user
      state.user.isLogged = true
    },
    loading (state, loading) {
      state.loading = loading
    },
    setUsers (state, users) {
      state.users = users
      // state.users = users.map(elm => ({
      //   ...elm,
      //   avatar:
      //     elm.avatar === ''
      //       ? ICONS[Math.Floor(Math.Random * ICONS.length + 1)]
      //       : elm.avatar
      // }))
    },
    setMessages (state, messages) {
      state.messages = messages
    },
    addMessage (state, message) {
      state.messages.push(message)
    },
    setTyper (state, { user, typing }) {
      const userObj = state.users.find(elm => user.username === elm.username)
      Vue.set(userObj, 'typing', typing)
    },
    poke (state) {
      state.poke++
    },
    depoke (state) {
      state.poke--
    },
    random (state) {
      state.messages = state.messages.sort(() => Math.random() - 0.5)
    }
    // updateDates (state) {
    //   state.messages.map(elm => {
    //     Vue.set(
    //       elm,
    //       'formattedTime',
    //       moment(elm.created)
    //         .locale('fr')
    //         .fromNow()
    //     )
    //   })
    // }
  },
  actions: {
    initiateLogin ({ commit }, user) {
      sock.emit('user register', user)
      commit('loading', true)
      console.log('Init login')
    },
    loginUser ({ commit }, user) {
      console.log('User registered')
      commit('loading', false)
      commit('loginUser', user)
      sessionStorage.setItem('username', user.username)
      sessionStorage.setItem('avatar', user.avatar)
      router.replace({ name: 'chat' })
    },
    usersUpdate ({ commit }, { joinedUser, users }) {
      commit('setUsers', users)
      console.log('A user joined the chat : ' + joinedUser.username)
    },
    messagesUpdate ({ commit, state }, { newMessage, messages }) {
      if (state.messages.length <= 0) {
        commit('setMessages', messages)
      } else {
        commit('addMessage', newMessage)
      }
    },
    setMessages ({ commit }, messages) {
      commit('setMessages', messages)
      //   commit('updateDates')
      //   setInterval(() => commit('updateDates'), 60 * 1000)
    },
    sendMessage (store, content) {
      sock.emit('message new', content)
    },
    type (store, bool) {
      sock.emit('user typing', bool)
    },
    setTyper ({ commit }, data) {
      commit('setTyper', data)
    },
    poke (store, username) {
      store.dispatch('sendMessage', '/poke ' + username)
    }
  }
})

sock.on('connect', () => {
  const user = store.state.user
  if (user.username !== null && typeof user.username !== 'undefined') {
    store.dispatch('initiateLogin', user)
  }
})

sock.on('user registered', user => store.dispatch('loginUser', user))
sock.on('users update', data =>
  store.dispatch('usersUpdate', { joinedUser: data.user, users: data.users })
)
sock.on('message new', data =>
  store.dispatch('messagesUpdate', {
    newMessage: data.message,
    messages: data.messages
  })
)
sock.on('messages update', data => store.dispatch('setMessages', data.messages))
sock.on('user typing', data =>
  store.dispatch('setTyper', {
    user: data.user,
    typing: data.typing
  })
)
sock.on('command new', data => {
  switch (data.command) {
    case 'poke':
      if (store.state.user.username === data.value) {
        store.commit('poke')
      }
      break
    case 'wizz':
      store.commit('poke')
      break
    case 'random':
      store.commit('random')
      break
    default:
      console.error('Unimplemented:', data)
  }
})
sock.on('chat error', error => {
  console.error(error)
  if (error.code === 101) {
    sessionStorage.clear()
    window.location.reload()
  }
})

export default store
